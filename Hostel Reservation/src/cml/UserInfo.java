package cml;

public class UserInfo {
	private static	String custname;
	  private String custid;
	  private String custphone;
	  private String custemail;
	  private String custfacebook;
	  private double custpayment;
	  private String bookid;
	
	  
	  
	  //getters
	  
	  public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public static String getCustname(){
		  return custname;
	  }
	  public String getCustid(){
		  return custid;
	  }
	  public String getCustphone(){
		  return custphone;
	  }
	  public String getCustemail(){
		  return custemail;
	  }
	  public String getCustfacebook(){
		  return custfacebook;
	  }
	  public double getCustpayment(){
		  return custpayment;
	  }
	  
	  //setters
	  
	  public static void setCustname(String custname1){
		  custname = custname1;
	  }
	  public void setCustid(String custid){
		  this.custid = custid;
	  }
	  public void setCustphone(String custphone){
		  this.custphone = custphone;
	  }
	  public void setCustemail(String custemail){
		  this.custemail = custemail;
	  }
	  public void setCustfacebook(String custfacebook){
		  this.custfacebook = custfacebook;
	  }
	  public void setCustname(double custpayment){
		  this.custpayment = custpayment;
	  }
	  
		
}
