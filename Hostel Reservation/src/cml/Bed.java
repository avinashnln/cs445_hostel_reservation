package cml;

import java.util.Date;

public class Bed {
 private int room;
 private int bed_id;
 private int price;
 private boolean isbooked=false;
 private String bed_available_date;
 
public int getRoom() {
	return room;
}
public void setRoom(int room) {
	this.room = room;
}
public int getBed_id() {
	return bed_id;
}
public void setBed_id(int bedId) {
	bed_id = bedId;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getDate() {
	return bed_available_date;
}
public void setDate(String bed_available_date){
	this.bed_available_date = bed_available_date;
}
public boolean isIsbooked() {
	return isbooked;
}
public void setIsbooked(boolean isbooked) {
	this.isbooked = isbooked;
}
}
