package cml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Search_facilitator {
	 String cityname;
	 String start_date;
	 String end_date;
	 List<Hostel> Hostel_Parsed_Detail;
	 List<Bed> bed_det;
	 private int search_id=1;
	
	public void search_parsed_xml(List Search_Command)
	{
		DataParser dp=new DataParser();
		Hostel_Parsed_Detail=dp.parser_xml_java();
		Iterator hi=Hostel_Parsed_Detail.iterator();
		Hostel h1;
		Bed b1;
		int c=0,l=0;
		while(hi.hasNext())
		{
		 h1=(Hostel)hi.next();
		 bed_det=h1.getBed_Detail();
		 Iterator bi=bed_det.iterator();
			 		 if(Search_Command.size()==1 && Search_Command.get(0).equals("search"))	
					      {
			 			        System.out.println("*************************Listing All Available Bed Details*******************************");
						        System.out.println(h1.getName()+" ,"+h1.getCity());  
						        
						        while(bi.hasNext() && bi.toString()!=null)	
						    	{
						        int flag=0;
						         b1=(Bed) bi.next();
						         String start_date= b1.getDate();
						         int year=Integer.parseInt(start_date.substring(0,4));
					   			 int month=Integer.parseInt(start_date.substring(4,6));
					   			 int date=Integer.parseInt(start_date.substring(6,8));
					   			  Calendar ca=Calendar.getInstance();
					   			  ca.set(year-100,month,date);
					   			  Date d1=ca.getTime();
					   			  
					   			
								try {
									File xmlFile = new File("Reservation.xml");
									if(xmlFile.exists())
									{
						   			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
						   			DocumentBuilder documentBuilder;
						   			Element rootElement;
									documentBuilder = documentFactory.newDocumentBuilder();
								
					   			    Document document;
										
										document = documentBuilder.parse(xmlFile);
								
									rootElement = document.createElement("search"); 
									 document.appendChild(rootElement); 
								
								   Document doc = documentBuilder.parse(xmlFile);
								
					   			 doc.getDocumentElement().normalize(); 
					   			 NodeList nodeList = doc.getElementsByTagName("Booking"); 
					   			 //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
					   			 for (int temp = 0; temp < nodeList.getLength(); temp++) 
					   			 { 
					   				 
					   				 Node node = nodeList.item(temp); 
					   			 //System.out.println("\nElement type :" + node.getNodeName()); 
					   			 if (node.getNodeType() == Node.ELEMENT_NODE) 
					   			 { Element searchDetail = (Element) node; 
					   			 
					   			   String hosname=searchDetail.getElementsByTagName("HostelName").item(0).getTextContent();
					   				String hcity=searchDetail.getElementsByTagName("city").item(0).getTextContent();
					   				String room=searchDetail.getElementsByTagName("room").item(0).getTextContent();
					   				int r1=Integer.parseInt(room);
					   				String price=searchDetail.getElementsByTagName("price").item(0) .getTextContent();
					   				int p1=Integer.parseInt(price);
					   				String cid=searchDetail.getElementsByTagName("CheckInDate").item(0).getTextContent();
					   			 if(hosname.equals(h1.getName())&& hcity.equals(h1.getCity())&&r1==b1.getRoom()&&p1==b1.getPrice()&&cid.equals(start_date)) 
					   				 flag=1;
					   			 }
					   			 }
									}
					   			 if(flag==0)
					   			 {
						    	System.out.println("-----------------------------------------------");
						    	System.out.println("Room Number : "+b1.getRoom());
						    	System.out.println("Bed Number: "+b1.getBed_id());
						    	System.out.println("Price for the Bed (Today) : "+b1.getPrice());
						    	System.out.println("Bed is available on : "+d1.getMonth()+"/"+d1.getDate()+"/"+d1.getYear());
					   			 }
						      }
						  
					   			  catch (SAXException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
								catch (ParserConfigurationException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}catch(FileNotFoundException ff){
								ff.printStackTrace();
						    	
						    	} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
					      }
					      }
			 		  else if(Search_Command.get(0).equals("search") && Search_Command.get(1).equals("--city") && Search_Command.size()==7) 
		  			   {
					 
						    cityname=(String) Search_Command.get(2);  
			   				start_date=(String) Search_Command.get(4);   
			   				end_date=(String) Search_Command.get(6); 
			   				int sdate=Integer.parseInt(start_date.substring(6,8));
			   			    int edate=Integer.parseInt(end_date.substring(6,8));
			   				String bed_avail_date;
			   				int flag=0,cnt=0;
			   				int end_date_str=Integer.parseInt(end_date.substring(6,8));
			   				Date d2;
			   			    System.out.println("************************* Listing Bed Details Between  "+start_date+" and  "+end_date+"******************************");
			   				System.out.println(h1.getName()+" ,"+h1.getCity());
						 	   					
			   			  while(bi.hasNext() && bi.toString()!=null)	
					    	{
					                 b1=(Bed) bi.next();
			  				    	// System.out.println("comparing date");
			  				    	 
				  				   	if(b1.getDate().equals(start_date))
				  				   	{
				   					   flag=1;
				  				   	   cnt++;}
				  				    else
				  				    {  sdate=Integer.parseInt(b1.getDate().substring(6,8));	
				  				             cnt++;}
				  				   	
//			   				    	 do
//			   				    	 {
			  				    	 if(flag==1 && sdate<edate)
			  				    	 {
						     		int year=Integer.parseInt(b1.getDate().substring(0,4));
					   			    int month=Integer.parseInt(b1.getDate().substring(4,6));
					   			    int date=Integer.parseInt(b1.getDate().substring(6,8));
					   			    
					   			   // System.out.println(year+" "+month +date);
					   			    Calendar cal1=Calendar.getInstance();
					   			    Calendar cal2=Calendar.getInstance();
					   			    cal1.set(year-100,month,date);
					   			    cal2.set(year-100,month,date);
					   			    cal2.add(Calendar.DATE,1);
					   			    Date d1=cal1.getTime();
					   			// System.out.println(d1.toString());
					   			     d2=cal2.getTime();
						    	    int bed_price=b1.getPrice();
						    	    System.out.println("-----------------------------------------------");
						    	    
						    	    System.out.println(d1.getMonth()+"/"+d1.getDate()+"/"+d1.getYear() +" to "+d2.getMonth()+"/"+d2.getDate()+"/"+d2.getYear()+" : $"+bed_price);
//						    		}while(d2.getDate()<end_date_str);
							      }
				  					  				   	
					    	}
		 				     
					    	}
			 		 
			 		 else if(Search_Command.get(0).equals("search") && Search_Command.get(1).equals("--city")&& Search_Command.get(7).equals("--beds") && Search_Command.size()==9)
			 		 {
			 			start_date=(String) Search_Command.get(4);   
		   				end_date=(String) Search_Command.get(6); 
		   			    int sdate=Integer.parseInt(start_date.substring(6,8));
		   			    int edate=Integer.parseInt(end_date.substring(6,8));
			 		     int flag=0;
			 			System.out.println(h1.getName()+" ,"+h1.getCity());
			 			 Search_read_writer srw=new Search_read_writer();
			   			  while(bi.hasNext() && bi.toString()!=null)	
					    	{
					                 b1=(Bed) bi.next();
					                 if(b1.getDate().equals(start_date))
					                 {
					   					   flag=1;
					   				 }
					                 else
					                 sdate=Integer.parseInt(b1.getDate().substring(6,8));
					                 
					                 if(flag==1 && sdate<edate)
					                 {
					                	 search_id=(new Random()).nextInt(9000) + 1000;
					                		 System.out.println("Search_id : "+search_id+", $"+b1.getPrice()+", room #"+b1.getRoom()+", "+b1.getDate());
					                		 srw.write_searchid_file(search_id,b1.getRoom(),b1.getPrice(),b1.getDate(),h1.getName(),h1.getCity());
					                  }
					                
					                
			 		 }
			   			System.out.println("Message::  File Saved to specified path :  searchedData.xml "); 
			  }
					 
		} 
		}
			

	}


	
	
