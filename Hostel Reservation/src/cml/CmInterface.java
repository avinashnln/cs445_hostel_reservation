package cml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.cli.*;

public class CmInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] searchArgs = null;
		List Search_Command=new ArrayList();
		Options options = new Options();
		//options.addOption("n", true, "[name] your name");
		//options.addOption(OptionBuilder.hasArg().withArgName("argname").create());
	
		//Option timeOption = new Option("t", false, "current time");
	//	options.addOption(OptionBuilder.hasArgs(2).hasOptionalArgs(4).withArgName("<Hostel> <command> [<city>] [<startdate>] [<enddate>] [<bed>] ")
			//	  .withValueSeparator(' ').create("search"));
		  Option searchApp = OptionBuilder.withArgName("HostelName command property1 value1 property2 value2 property3 value3 property4 value4")
	            .withValueSeparator(' ')
	            .hasArgs(2)
	            .hasOptionalArgs(10)
	            .withDescription("You can Search with the Following Option.")
	            .create("h21");
		  options.addOption(searchApp);
		// ** now lets parse the input
		CommandLineParser parser = new BasicParser();
		CommandLine cmd;
		try{
		cmd = parser.parse(options,args);
		 searchArgs = cmd.getOptionValues("h21");
		}catch (ParseException pe)
		{ usage(options); return; }
		catch(NullPointerException ne)
	     { 
			ne.printStackTrace();
	     }

	
		try
		{
		//System.out.println(cmd.getArgList());
		for(int i=0;i<searchArgs.length;i++)
		{
		  Search_Command.add(searchArgs[i]);
		}
		
		  //while(true) {      
			  //Infinite Loop
		
		    System.out.println("*******************Welcome To Hostel 21********************************");
		    
		    if(Search_Command.get(0).equals("admin"))
            {
		    	if(Search_Command.get(1).equals("load"))
		    	{
		    	System.out.println("You have given the command as : "+ Search_Command.get(0));
         	   Admin ad=new Admin();
        	   ad.loadxml(Search_Command);
        	   System.exit(0);
		    	}
		    	else if (Search_Command.get(1).equals("occupancy"))
		    	{
		    		Book_bed bb=new Book_bed();
		    		bb.check_oocupany();
		    	}
		    	
		    	else if (Search_Command.get(1).equals("revenue"))
		    	{
		    		Book_bed bb=new Book_bed();
		    		bb.check_revenue();
		    	}
            }
		    else if(Search_Command.get(0).equals("search"))
		    		{
		    	System.out.println("You have given the command as : "+ Search_Command.get(0));
		      Search_facilitator sf=new Search_facilitator();
			  sf.search_parsed_xml(Search_Command);
			  System.exit(0);
		    		}
			 
		    else if(Search_Command.get(0).equals("book") && Search_Command.get(1).equals("add"))
               {
		    	 System.out.println("You have given the command as : "+ Search_Command.get(0));
        		  Book_bed bb=new Book_bed();
  			      bb.validate_search_user(Search_Command);
  			    System.exit(0);
  			  }
		     
		     
		    else if(Search_Command.get(0).equals("user") && Search_Command.get(1).equals("add"))
             {
		    	System.out.println("You have given the command as : "+ Search_Command.get(0));
        	   userwrfile urw=new userwrfile();
        	   System.out.println((String)Search_Command.get(7));
        	   urw.read_from_file((String)Search_Command.get(7));
			   urw.write_to_file(Search_Command);
			   System.exit(0);
			  }
		    
		    else if(Search_Command.get(0).equals("user") && Search_Command.get(1).equals("sendmail"))
            {
		    	System.out.println("You have given the command as : "+ Search_Command.get(0));
       	        String mailid=(String)Search_Command.get(2);
       	         String msg=(String)Search_Command.get(3);
       	      ComposeMail cm=new ComposeMail();
       	      cm.writemailtofile(mailid,msg);
			   System.exit(0);
			  }
		    
		    else if(Search_Command.get(0).equals("user") && Search_Command.get(1).equals("readmail"))
            {
		    	System.out.println("You have given the command as : "+ Search_Command.get(0));
       	       	      ComposeMail cm=new ComposeMail();
       	            cm.reademailfile();
			   System.exit(0);
			  }
			 
		    else
		    {
			  System.out.println("Oops...No Matching  Input Provided....");
		     System.exit(0);
		      }     
		}catch(NullPointerException ne)
		     { 
			   
			System.out.println("You have entered NULL...No Matching  Input Provided....");
		     }
	//}
	}
		private static void usage(Options options){

		// Use the inbuilt formatter class
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "CLI for Hostel Reservation : Options", options );
		
	         }

}

