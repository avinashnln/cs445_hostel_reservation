package cml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Search_read_writer {

	DocumentBuilderFactory documentFactory;
	DocumentBuilder documentBuilder;
	Document document;
	Element rootElement;
	String filename;
	Search_read_writer()
	{
		try {
		  documentFactory = DocumentBuilderFactory.newInstance(); 
		  documentBuilder = documentFactory.newDocumentBuilder(); 
			// define root elements 
		 File  filename= new File("searchedData.xml");
		  if(!filename.exists())
		  {
		     document = documentBuilder.newDocument(); 
		     rootElement = document.createElement("search"); 
			 document.appendChild(rootElement);
		  }
		  else
			  
		  {
			    document = documentBuilder.parse(filename);
			     rootElement = document.createElement("search"); 
				 document.appendChild(rootElement); 
		  }
		} catch (ParserConfigurationException pce) { 
			pce.printStackTrace(); 
			} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public ArrayList read_searchid_file(String s)
	{
		String search_id=null;
		String hosname=null;
		String hcity=null;
		String room=null;
		String price=null;
		String  cid=null;
	    ArrayList search_user=new ArrayList();
		try {
			
		File xmlFile = new File("searchedData.xml");

		Document doc = documentBuilder.parse(xmlFile); 
		 doc.getDocumentElement().normalize(); 
		 NodeList nodeList = doc.getElementsByTagName("searchDetail"); 
		 //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		 for (int temp = 0; temp < nodeList.getLength(); temp++) 
		 { 
			 Node node = nodeList.item(temp); 
		 //System.out.println("\nElement type :" + node.getNodeName()); 
		 if (node.getNodeType() == Node.ELEMENT_NODE) 
		 { Element searchDetail = (Element) node; 
		 search_id=searchDetail.getAttribute("id");
		 if(s.equals(search_id))
		 {
			hosname=searchDetail.getElementsByTagName("HostelName").item(0).getTextContent();
			hcity=searchDetail.getElementsByTagName("city").item(0).getTextContent();
			room=searchDetail.getElementsByTagName("room").item(0).getTextContent();
			price=searchDetail.getElementsByTagName("price").item(0) .getTextContent();
			cid=searchDetail.getElementsByTagName("CheckInDate").item(0).getTextContent();
			search_user.add(search_id);
			search_user.add(hosname);
			search_user.add(hcity);
			search_user.add(room);
			search_user.add(price);
			search_user.add(cid);
		 }
		 
		    //ufname=custDetail.getElementsByTagName("firstname").item(0) .getTextContent();
	     }
	
		 }
		 
		} catch (Exception e) { e.printStackTrace(); } 
		return search_user;
	}
	
	public void write_searchid_file(int search_id,int room,int price,String cdate,String hname,String city)
	{
	try { 
		String lsearch_id=Integer.toString(search_id);
		String lroom=Integer.toString(room);
		String lprice=Integer.toString(price);
	
	// define school elements 
		 
	Element search = document.createElement("searchDetail"); 
	rootElement.appendChild(search); 
	// add attributes to school 
	
	Attr attribute = document.createAttribute("id"); 
	attribute.setValue(lsearch_id);
	search.setAttributeNode(attribute);
	
	Element hosname = document.createElement("HostelName"); 
	hosname.appendChild(document.createTextNode(hname)); 
	search.appendChild(hosname); 
	// lastname elements 
	Element hoscity = document.createElement("city"); 
	hoscity.appendChild(document.createTextNode(city)); 
	search.appendChild(hoscity);
	
	// firstname elements 
	Element eroom = document.createElement("room"); 
	eroom.appendChild(document.createTextNode(lroom)); 
	search.appendChild(eroom); 
	// lastname elements 
	Element eprice = document.createElement("price"); 
	eprice.appendChild(document.createTextNode(lprice)); 
	search.appendChild(eprice); 
	// email elements 
	Element edate = document.createElement("CheckInDate"); 
	edate.appendChild(document.createTextNode(cdate)); 
	search.appendChild(edate); 
	
	// creating and writing to xml file 
	TransformerFactory transformerFactory = TransformerFactory.newInstance(); 
	Transformer transformer = transformerFactory.newTransformer(); 
	DOMSource domSource = new DOMSource(document); 
	StreamResult streamResult = new StreamResult(new File("searchedData.xml")); 
	transformer.transform(domSource, streamResult); 
	} catch (TransformerException tfe) { 
	tfe.printStackTrace(); 
	} 

	}

}
