package cml;

import java.util.List;

public class Hostel {
private String name;
private String street;
private String city;
private String state;
private String Postal_Code;
private String Country;
private int Phone;
private String email;
private String facebook;
private String web;
private char smoking;
private char alcohol;
private String checkintime;
private String checkouttime;
private int cancellation_deadline;
private String cancellation_penalty;
private List<Bed> Bed_Detail;


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getStreet() {
	return street;
}
public void setStreet(String street) {
	this.street = street;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getPostal_Code() {
	return Postal_Code;
}
public void setPostal_Code(String postalCode) {
	Postal_Code = postalCode;
}
public String getCountry() {
	return Country;
}
public void setCountry(String country) {
	Country = country;
}
public int getPhone() {
	return Phone;
}
public void setPhone(int phone) {
	Phone = phone;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getFacebook() {
	return facebook;
}
public void setFacebook(String facebook) {
	this.facebook = facebook;
}
public String getWeb() {
	return web;
}
public void setWeb(String web) {
	this.web = web;
}
public char getSmoking() {
	return smoking;
}
public void setSmoking(char smoking) {
	this.smoking = smoking;
}
public char getAlcohol() {
	return alcohol;
}
public void setAlcohol(char alcohol) {
	this.alcohol = alcohol;
}
public String getCheckintime() {
	return checkintime;
}
public void setCheckintime(String checkintime) {
	this.checkintime = checkintime;
}
public String getCheckouttime() {
	return checkouttime;
}
public void setCheckouttime(String checkouttime) {
	this.checkouttime = checkouttime;
}
public int getCancellation_deadline() {
	return cancellation_deadline;
}
public void setCancellation_deadline(int cancellation_deadline) {
	this.cancellation_deadline = cancellation_deadline;
}
public String getCancellation_penalty() {
	return cancellation_penalty;
}
public void setCancellation_penalty(String cancellation_penalty) {
	this.cancellation_penalty = cancellation_penalty;
}
public List<Bed> getBed_Detail() {
	return Bed_Detail;
}
public void setBed_Detail(List<Bed> bed_Detail) {
		Bed_Detail = bed_Detail;
}


}


