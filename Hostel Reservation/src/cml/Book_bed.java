package cml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Book_bed {
	DocumentBuilderFactory documentFactory;
	DocumentBuilder documentBuilder;
	Document document;
	Element rootElement;
	String user_id=null;
	ArrayList ar;
	Book_bed()
	{
		 documentFactory = DocumentBuilderFactory .newInstance(); 
		 try {
			documentBuilder = documentFactory .newDocumentBuilder();
					
			 File  filename= new File("Reservation.xml");
			  if(!filename.exists())
			  {
			     document = documentBuilder.newDocument(); 
			     
			  }
			  else
				  
			  {
				    document = documentBuilder.parse(filename);
				     
			  }
			} catch (ParserConfigurationException pce) { 
				pce.printStackTrace(); 
				} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
	
	}
	
public void validate_search_user(List search_Command)
{
	if(search_Command.get(2).equals("--search_id") && search_Command.get(4).equals("--user_id"))
    {
		ar=new ArrayList();
   System.out.println("*******Restoring Saved search_id*********");
	String search_id=(String) search_Command.get(3);
    user_id=(String) search_Command.get(5);
	Search_read_writer srw=new Search_read_writer();
	ar=srw.read_searchid_file(search_id);
	write_booking_xml();
  }
	
	}

public void write_booking_xml()
{
	
	String hname=(String) ar.get(1);
	String hcity=(String) ar.get(2);
	String room=(String) ar.get(3);
	String price=(String) ar.get(4);
	String cid=(String) ar.get(5);
	try { 
		
	// define school elements 
		int b_id=(new Random()).nextInt(9000) + 1000;
		int i=0;
		String booking_id=Integer.toString(b_id);
		rootElement = document.createElement("Reservation"); 
		 document.appendChild(rootElement); 
		Element Reservation = document.createElement("Booking"); 
		rootElement.appendChild(Reservation);  
	// add attributes to school 
		Attr attribute = document.createAttribute("id"); 
		attribute.setValue(booking_id);
		Reservation.setAttributeNode(attribute);
		
		Element cusid = document.createElement("CustomerID"); 
		cusid.appendChild(document.createTextNode(user_id)); 
		Reservation.appendChild(cusid); 
		
		Element hosname = document.createElement("HostelName"); 
		hosname.appendChild(document.createTextNode(hname)); 
		Reservation.appendChild(hosname); 
		// lastname elements 
		Element hoscity = document.createElement("city"); 
		hoscity.appendChild(document.createTextNode(hcity)); 
		Reservation.appendChild(hoscity);
		
		// firstname elements 
		Element eroom = document.createElement("room"); 
		eroom.appendChild(document.createTextNode(room)); 
		Reservation.appendChild(eroom); 
		// lastname elements 
		Element eprice = document.createElement("price"); 
		eprice.appendChild(document.createTextNode(price)); 
		Reservation.appendChild(eprice); 
	// email elements 
	
		Element checkdate = document.createElement("CheckInDate"); 
		checkdate.appendChild(document.createTextNode(cid)); 
		Reservation.appendChild(checkdate); 
		
		  
	// creating and writing to xml file 
	TransformerFactory transformerFactory = TransformerFactory.newInstance(); 
	Transformer transformer = transformerFactory.newTransformer(); 
	DOMSource domSource = new DOMSource(document); 
	StreamResult streamResult = new StreamResult(new File("Reservation.xml")); 
	transformer.transform(domSource, streamResult); 
	int year=Integer.parseInt(cid.substring(0,4));
	    int month=Integer.parseInt(cid.substring(4,6));
	    int date=Integer.parseInt(cid.substring(6,8));
	    
	   // System.out.println(year+" "+month +date);
	    Calendar cal1=Calendar.getInstance();
	    Calendar cal2=Calendar.getInstance();
	    cal1.set(year-100,month,date);
	    cal2.set(year-100,month,date);
	    cal2.add(Calendar.DATE,1);
	    Date d1=cal1.getTime();
	// System.out.println(d1.toString());
	     Date d2=cal2.getTime();
	     ++i;
	  System.out.println("Booking successful! Here's the detail of your booking:");
	  System.out.println();
	  System.out.println(hname+" , "+hcity);
	  System.out.println("Check-in date: "+d1.getMonth()+"/"+d1.getDate()+"/"+d1.getYear());
	  System.out.println("Check-Out date: "+d2.getMonth()+"/"+d2.getDate()+"/"+d2.getYear());
	  System.out.println(" Beds : "+i);
	  System.out.println("Booking ID : " +booking_id);
	  System.out.println("User_Id : " +user_id);
	  System.out.println("Price : $"+price);
	
	} catch (TransformerException tfe) { 
	tfe.printStackTrace(); 
	} 

	}

  public void check_oocupany()
  {
		
		try {
			File xmlFile = new File("Reservation.xml");
			if(xmlFile.exists())
			{
 			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
 			DocumentBuilder documentBuilder;
 			Element rootElement;
			documentBuilder = documentFactory.newDocumentBuilder();
		
			    Document document;
				
				document = documentBuilder.parse(xmlFile);
		
			rootElement = document.createElement("search"); 
			 document.appendChild(rootElement); 
		
		   Document doc = documentBuilder.parse(xmlFile);
		
			 doc.getDocumentElement().normalize(); 
			 NodeList nodeList = doc.getElementsByTagName("Booking"); 
			 //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			 for (int temp = 0; temp < nodeList.getLength(); temp++) 
			 { 
				 
				 Node node = nodeList.item(temp); 
			 //System.out.println("\nElement type :" + node.getNodeName()); 
			 if (node.getNodeType() == Node.ELEMENT_NODE) 
			 { Element searchDetail = (Element) node; 
			 
			   String hosname=searchDetail.getElementsByTagName("HostelName").item(0).getTextContent();
				String hcity=searchDetail.getElementsByTagName("city").item(0).getTextContent();
				String room=searchDetail.getElementsByTagName("room").item(0).getTextContent();
				String price=searchDetail.getElementsByTagName("price").item(0) .getTextContent();
				String cid=searchDetail.getElementsByTagName("CheckInDate").item(0).getTextContent();
				int year=Integer.parseInt(cid.substring(0,4));
   			    int month=Integer.parseInt(cid.substring(4,6));
   			    int date=Integer.parseInt(cid.substring(6,8));
   			    
   			   // System.out.println(year+" "+month +date);
   			    Calendar cal1=Calendar.getInstance();
   			    cal1.set(year-100,month,date);
   			    Date d1=cal1.getTime();
				System.out.println("-------------Hostel OccuPancy Detail----------------------------------");
			  	System.out.println("Hostel Name : "+hosname);
			  	System.out.println("City : "+hcity);
			  	System.out.println("Room Number : "+room);
			   	System.out.println("Price for the Bed (Today) : "+price);
			  	System.out.println("Bed is Occupied  on : "+d1.getMonth()+"/"+d1.getDate()+"/"+d1.getYear());
  	
			 }
			 }
    }
			}

			  catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		catch (ParserConfigurationException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}catch(FileNotFoundException ff){
		ff.printStackTrace();
  	
  	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
  
  public void check_revenue()
  {
	  int revenue=0;
	  try {
			File xmlFile = new File("Reservation.xml");
			if(xmlFile.exists())
			{
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder documentBuilder;
			Element rootElement;
			documentBuilder = documentFactory.newDocumentBuilder();
		
			    Document document;
				
				document = documentBuilder.parse(xmlFile);
		
			rootElement = document.createElement("search"); 
			 document.appendChild(rootElement); 
		
		   Document doc = documentBuilder.parse(xmlFile);
		
			 doc.getDocumentElement().normalize(); 
			 NodeList nodeList = doc.getElementsByTagName("Booking"); 
			 //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			 for (int temp = 0; temp < nodeList.getLength(); temp++) 
			 { 
				 
				 Node node = nodeList.item(temp); 
			 //System.out.println("\nElement type :" + node.getNodeName()); 
			 if (node.getNodeType() == Node.ELEMENT_NODE) 
			 { Element searchDetail = (Element) node; 
			 
			   String hosname=searchDetail.getElementsByTagName("HostelName").item(0).getTextContent();
				String hcity=searchDetail.getElementsByTagName("city").item(0).getTextContent();
				String price=searchDetail.getElementsByTagName("price").item(0) .getTextContent();
				int p1=Integer.parseInt(price);
				String cid=searchDetail.getElementsByTagName("CheckInDate").item(0).getTextContent();
				int year=Integer.parseInt(cid.substring(0,4));
 			    int month=Integer.parseInt(cid.substring(4,6));
 			    int date=Integer.parseInt(cid.substring(6,8));
 			    
 			   // System.out.println(year+" "+month +date);
 			    Calendar cal1=Calendar.getInstance();
 			    cal1.set(year-100,month,date);
 			    Date d1=cal1.getTime();
 			    
 			     revenue=revenue+p1;
				System.out.println("-------------Hostel Revenue Detail----------------------------------");
			  	System.out.println(hosname+" is generated Revenue of $" + revenue + "  on  "+d1.getMonth()+"/"+d1.getDate()+"/"+d1.getYear()
			  			+ " in city "+ hcity);
			  
	
			 }
			 }
  }
			}

			  catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		catch (ParserConfigurationException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}catch(FileNotFoundException ff){
		ff.printStackTrace();
	
	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
}



