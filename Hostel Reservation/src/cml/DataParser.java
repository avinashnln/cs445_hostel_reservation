package cml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DataParser 
{
	List<Hostel> Hostel_Detail;
	List<Bed> Bed_Detail;
	Hostel hostel1;
	Bed bed;
	
		public List<Hostel> parser_xml_java()
	{
	//private void parseXmlFile(){
		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			
		//Using factory get an instance of document builder
		DocumentBuilder db = dbf.newDocumentBuilder();
		//parse using builder to get DOM representation of the XML file
		Document  dom = db.parse("data.xml");
		
		 Hostel_Detail = new ArrayList<Hostel>();  
		 Bed_Detail = new ArrayList<Bed>(); 
  //Iterating through the nodes and extracting the data.  

		  NodeList nodeList = dom.getDocumentElement().getChildNodes();  
		  hostel1 = new Hostel(); 
	      for (int i = 0; i < nodeList.getLength(); i++) 
	       {  
      //We have encountered an <Hostel > tag.  
           Node node = nodeList.item(i);  
              if (node instanceof Element) 
             {  
            	   //creating  Hostel Object
            	  bed=new Bed();
             //hostel1.id = node.getAttributes().  if any element has their Attribute and value
	         //getNamedItem("id").getNodeValue();  
              String nodecontent = node.getLastChild().getTextContent().trim(); 
              if(node.getNodeName().equals("name"))
 	         {
 	    	     //hostel1.setName(content);  
 	    	 hostel1.setName(nodecontent); 
 	         }
             NodeList childNodes = node.getChildNodes(); 
                 for (int j = 0; j < childNodes.getLength(); j++) 
	           {  
              Node cNode = childNodes.item(j);  
           //Identifying the child tag of Hostel.. 
               // System.out.println(cNode.getNodeName());
           if (cNode instanceof Element) 
           {  
        	
	       String content = cNode.getLastChild().getTextContent().trim();  
	          
	       if(cNode.getNodeName().equals("street"))
	         {
	    	    hostel1.setStreet(content);  
	           }
	        if(cNode.getNodeName().equals("city"))
	         {
	    	 hostel1.setCity(content);  
	           }
	       if(cNode.getNodeName().equals("state"))
	         {
	    	   hostel1.setState(content);  
	           }
	       if(cNode.getNodeName().equals("Postal_Code"))
	         {
	    	   hostel1.setPostal_Code(content);  
	           }
	       if(cNode.getNodeName().equals("Country"))
	         {
	    	   hostel1.setCountry(content);  
	           }
	       if(cNode.getNodeName().equals("Phone"))
	         {
	    	   int phone=Integer.parseInt(content);
	    	   hostel1.setPhone(phone);  
	           }
	       if(cNode.getNodeName().equals("email"))
	         {
	    	   hostel1.setEmail(content);  
	          }
	       if(cNode.getNodeName().equals("facebook"))
	         {
	    	   hostel1.setFacebook(content);  
	           }
	       if(cNode.getNodeName().equals("web"))
	         {
	    	   hostel1.setWeb(content);  
	           }
	       
	       if(cNode.getNodeName().equals("smoking"))
	         {
	    	   char smoking=content.charAt(0);
	    	   hostel1.setSmoking(smoking);  
	           }
	       if(cNode.getNodeName().equals("alcohol"))
	         {
	    	   char alcohol=content.charAt(0);
	    	   hostel1.setAlcohol(alcohol);  
	           }
	       if(cNode.getNodeName().equals("cancellation_deadline"))
	         {
	    	   int cancellation_dead=Integer.parseInt(content);
	    	 	  hostel1.setCancellation_deadline(cancellation_dead);
	           }
	       if(cNode.getNodeName().equals("cancellation_penalty"))
	         {
	    	   //int cancellation_pen=Integer.parseInt(content);
	    	 	  hostel1.setCancellation_penalty(content);
	           }
	       
	          if(cNode.getNodeName().equals("date") && (content!=null))
		         {
		    	 	  bed.setDate(content);
		           }
 	   	         if(cNode.getNodeName().equals("room") && (content!=null))
	           {
	    	   int room=Integer.parseInt(content);
	    	 	 bed.setRoom(room);
	            }
	       
	       if(cNode.getNodeName().equals("bed")&& (content!=null))
	         {
	    	     int b1=Integer.parseInt(content);
	    	      bed.setBed_id(b1);
	           }
	       
	       if(cNode.getNodeName().equals("price")&& (content!=null))
	         {
	    	   int price=Integer.parseInt(content);
	    	      bed.setPrice(price);
	         } 
           }   
             }
                 if(bed.getBed_id()!=0 && bed.getRoom()!=0 &&bed.getDate()!=null && bed.getPrice()!=0)
                          Bed_Detail.add(bed);
                 hostel1.setBed_Detail(Bed_Detail);   
                    }
                }
	      Hostel_Detail.add(hostel1);
	     
	   	} catch(ParserConfigurationException pce)
		{
		pce.printStackTrace();
		} catch(SAXException se) {
		se.printStackTrace();
		} catch(IOException ioe) {
		ioe.printStackTrace();
		} catch(NullPointerException ne)
		{
		ne.printStackTrace();
		}
		 
		return Hostel_Detail;
	
}
		

		public List<Hostel> replace_load_xml_(String f)
	{
	//private void parseXmlFile(){
		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			File filename=new File(f);
		//Using factory get an instance of document builder
		DocumentBuilder db = dbf.newDocumentBuilder();
		//parse using builder to get DOM representation of the XML file
		Document  dom = db.parse(filename);
		
		 Hostel_Detail = new ArrayList<Hostel>();  
		 Bed_Detail = new ArrayList<Bed>(); 
  //Iterating through the nodes and extracting the data.  

		  NodeList nodeList = dom.getDocumentElement().getChildNodes();  
		  hostel1 = new Hostel(); 
	      for (int i = 0; i < nodeList.getLength(); i++) 
	       {  
      //We have encountered an <Hostel > tag.  
           Node node = nodeList.item(i);  
              if (node instanceof Element) 
             {  
            	   //creating  Hostel Object
            	  bed=new Bed();
             //hostel1.id = node.getAttributes().  if any element has their Attribute and value
	         //getNamedItem("id").getNodeValue();  
              String nodecontent = node.getLastChild().getTextContent().trim(); 
              if(node.getNodeName().equals("name"))
 	         {
 	    	     //hostel1.setName(content);  
 	    	 hostel1.setName(nodecontent); 
 	         }
             NodeList childNodes = node.getChildNodes(); 
                 for (int j = 0; j < childNodes.getLength(); j++) 
	           {  
              Node cNode = childNodes.item(j);  
           //Identifying the child tag of Hostel.. 
               // System.out.println(cNode.getNodeName());
           if (cNode instanceof Element) 
           {  
        	
	       String content = cNode.getLastChild().getTextContent().trim();  
	          
	       if(cNode.getNodeName().equals("street"))
	         {
	    	    hostel1.setStreet(content);  
	           }
	        if(cNode.getNodeName().equals("city"))
	         {
	    	 hostel1.setCity(content);  
	           }
	       if(cNode.getNodeName().equals("state"))
	         {
	    	   hostel1.setState(content);  
	           }
	       if(cNode.getNodeName().equals("Postal_Code"))
	         {
	    	   hostel1.setPostal_Code(content);  
	           }
	       if(cNode.getNodeName().equals("Country"))
	         {
	    	   hostel1.setCountry(content);  
	           }
	       if(cNode.getNodeName().equals("Phone"))
	         {
	    	   int phone=Integer.parseInt(content);
	    	   hostel1.setPhone(phone);  
	           }
	       if(cNode.getNodeName().equals("email"))
	         {
	    	   hostel1.setEmail(content);  
	          }
	       if(cNode.getNodeName().equals("facebook"))
	         {
	    	   hostel1.setFacebook(content);  
	           }
	       if(cNode.getNodeName().equals("web"))
	         {
	    	   hostel1.setWeb(content);  
	           }
	       
	       if(cNode.getNodeName().equals("smoking"))
	         {
	    	   char smoking=content.charAt(0);
	    	   hostel1.setSmoking(smoking);  
	           }
	       if(cNode.getNodeName().equals("alcohol"))
	         {
	    	   char alcohol=content.charAt(0);
	    	   hostel1.setAlcohol(alcohol);  
	           }
	       if(cNode.getNodeName().equals("cancellation_deadline"))
	         {
	    	   int cancellation_dead=Integer.parseInt(content);
	    	 	  hostel1.setCancellation_deadline(cancellation_dead);
	           }
	       if(cNode.getNodeName().equals("cancellation_penalty"))
	         {
	    	   //int cancellation_pen=Integer.parseInt(content);
	    	 	  hostel1.setCancellation_penalty(content);
	           }
	       
	          if(cNode.getNodeName().equals("date") && (content!=null))
		         {
		    	 	  bed.setDate(content);
		           }
 	   	         if(cNode.getNodeName().equals("room") && (content!=null))
	           {
	    	   int room=Integer.parseInt(content);
	    	 	 bed.setRoom(room);
	            }
	       
	       if(cNode.getNodeName().equals("bed")&& (content!=null))
	         {
	    	     int b1=Integer.parseInt(content);
	    	      bed.setBed_id(b1);
	           }
	       
	       if(cNode.getNodeName().equals("price")&& (content!=null))
	         {
	    	   int price=Integer.parseInt(content);
	    	      bed.setPrice(price);
	         } 
           }   
             }
                 if(bed.getBed_id()!=0 && bed.getRoom()!=0 &&bed.getDate()!=null && bed.getPrice()!=0)
                          Bed_Detail.add(bed);
                 hostel1.setBed_Detail(Bed_Detail);   
                    }
                }
	      Hostel_Detail.add(hostel1);
	     
	   	} catch(ParserConfigurationException pce)
		{
		pce.printStackTrace();
		} catch(SAXException se) {
		se.printStackTrace();
		} catch(IOException ioe) {
		ioe.printStackTrace();
		} catch(NullPointerException ne)
		{
		ne.printStackTrace();
		}
		
		 
		return Hostel_Detail;
	
}
		
}
